<?php

class DoubekZ1U1{
	public function stringGetPermutations($prefix, $char, &$permutations){
		if (count($char) == 1){
			$permutations[] = $prefix . array_pop($char);
		}
		else{
			for ($i = 0; $i < count($char); $i++){
				$tmp = $char;
				unset($tmp[$i]);
				$this->stringGetPermutations($prefix . $char[$i], array_values($tmp), $permutations);
			}
		}
	}

	public function countPermutation($input){
		$char = array();
		for ($i = 0; $i < strlen($input); $i++)
			$char[] = $input[$i];
		$permutations = array();

		$this->stringGetPermutations("", $char, $permutations);

		return count($permutations);
	}

	public function getOffset($input){
		$rst = $this->countPermutation($input) % 26;
		return $rst;
	}

	public function caesarCipher($str, $pass){
		//osetri vstupy
		$str = strtoupper($str);
		$str = str_replace(" ", "", $str);
		$str = str_replace("-", "", $str);
		//abeceda
		$alphabet=array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
		//array flip
		$flip=array_flip($alphabet);
		$text = $str;
		$text=str_split($text); 	//rozbi vstupny reatazec
		$n=count($text);			//pocet znakov
		$encrypted_text='';         //encrypted_text 
		
		for ($i=0; $i<$n; $i++){
			$encrypted_text.=$alphabet[($flip[$text[$i]]+$pass)%26]; //algoritmus cesarovej sifry
		}
		return $encrypted_text;	
	} 

	public function run(){
		$str 	= "jednoduche-sifrovanie";
		$pass 	= $this->getOffset("doubek");
		print $this->caesarCipher($str, $pass);
	}
}